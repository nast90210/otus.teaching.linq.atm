﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");
            System.Console.WriteLine();
            var atmManager = CreateATMManager();
            
            //TODO: Далее выводим результаты разработанных LINQ запросов
            
            System.Console.WriteLine("Вывод информации по заданию 1");
            var user = atmManager.GetUser("snow", "111");
            System.Console.WriteLine(user.ToString());
            System.Console.WriteLine();

            System.Console.WriteLine("Вывод информации по заданию 2");
            var accounts = atmManager.GetUserAccounts(user);
            foreach (var account in accounts)
            {
                System.Console.WriteLine(account.ToString());
            }
            System.Console.WriteLine();
            
            System.Console.WriteLine("Вывод информации по заданию 3");
            var accountWithHistory = atmManager.GetUserAccountsWithHistories(user);
            foreach (KeyValuePair<Account,List<OperationsHistory>> keyValuePair in accountWithHistory)
            {
                System.Console.WriteLine(keyValuePair.Key.ToString());
                foreach (var operationsHistory in keyValuePair.Value)
                {
                    System.Console.WriteLine(operationsHistory.ToString());
                }
            }
            System.Console.WriteLine();
            
            System.Console.WriteLine("Вывод информации по заданию 4");
            var inputCashOperationsHistories = atmManager.GetAllInputCashOperationsHistories();
            foreach (KeyValuePair<OperationsHistory,User> inputCashOperationsHistory in inputCashOperationsHistories)
            {
                System.Console.WriteLine(inputCashOperationsHistory.Key.ToString());
                System.Console.WriteLine("Владелец счета: ");
                System.Console.WriteLine(inputCashOperationsHistory.Value.ToString());
            }
            System.Console.WriteLine();
            
            System.Console.WriteLine("Вывод информации по заданию 5");
            var usersCollectionWithCashMoreThanN = atmManager.GetUsersCollectionWithCashMoreThanN(10000);
            foreach (var userWithCashMoreThanN in usersCollectionWithCashMoreThanN)
            {
                System.Console.WriteLine(userWithCashMoreThanN.ToString());
            }

            System.Console.WriteLine();
            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}