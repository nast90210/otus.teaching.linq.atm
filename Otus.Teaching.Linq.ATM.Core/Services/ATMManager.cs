﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        //TODO: Добавить методы получения данных для банкомата

        public User GetUser(string userLogin, string userPassword)
        {
            var user = Users.SingleOrDefault(x => x.Password == userPassword && x.Login == userLogin);

            return user ?? throw new ArgumentException("Не правильный логин или пароль");
        }

        public List<Account> GetUserAccounts(User user)
        {
            var accounts = Accounts.Where(account => account.UserId == user.Id).ToList();

            return accounts.Count > 0 ? accounts : throw new ArgumentException($"У пользователя c id {user.Id} нет открытых счетов");
        }

        public Dictionary<Account, List<OperationsHistory>> GetUserAccountsWithHistories(User user)
        {
            var result = (from account in Accounts
                where account.UserId == user.Id
                join history in History on account.Id equals history.AccountId into coll
                select new {Account = account, History = coll}).ToDictionary(arg => arg.Account, arg => arg.History.ToList());

            return result.Count > 0 ? result : throw new ArgumentException($"У пользователя c id {user.Id} нет открытых счетов"); 
        }

        public Dictionary<OperationsHistory, User> GetAllInputCashOperationsHistories()
        {
            if (!History.Any())
                throw new ArgumentException("Нет операций для вывода");

            var result = (from operationsHistory in History
                    where operationsHistory.OperationType == OperationType.InputCash
                    join account in Accounts on operationsHistory.AccountId equals account.Id
                    join user in Users on account.UserId equals user.Id
                    select new {OperationsHistory = operationsHistory, User = user})
                .ToDictionary(arg => arg.OperationsHistory, arg => arg.User);
            
            
            return result.Count > 0 ? result : throw new ArgumentException("Нет операций пополнения счета");
        }

        public List<User> GetUsersCollectionWithCashMoreThanN(int N)
        {
            var result = (from account in Accounts
                where account.CashAll > N
                join user in Users on account.UserId equals user.Id
                select user).Distinct().ToList();


            return result.Count > 0 ? result : throw new ArgumentException($"Нет клиентов, у которых на счету больше {N}");
        }
        
    }
}